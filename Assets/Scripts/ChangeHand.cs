﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeHand : MonoBehaviour
{
    public float scaleX;
    public float scaleY;
    public float scaleZ;
    //Transform handScale = GameObject.Find("CapsuleHand").transform.localScale;

    public void switchHand()
    {
        //handScale =  handScale * -1;
        scaleX = GameObject.Find("CapsuleHand").transform.localScale.x;
        scaleY = GameObject.Find("CapsuleHand").transform.localScale.y;
        scaleZ = GameObject.Find("CapsuleHand").transform.localScale.z;
        Vector3 v = new Vector3(scaleX * -1F, scaleY, scaleZ);
        GameObject.Find("CapsuleHand").transform.localScale = v;
    }
}
