﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
public class RestartButton : MonoBehaviour
{
    public int task = 1;
    public int tempTask = 0;

    //public GameObject theCube = GameObject.Find("DeskBox");
    public Renderer rend;
    public Color newColor = Color.red;
    bool canVib = true;

    public void NoVib()
    {
        //GameObject theCube = GameObject.Find("DeskBox");
        NewTask(task);
        task++;
    }

    public void NewTask(int tasks)
    {
        GameObject theCube = GameObject.Find("DeskBox");
        GameObject fakeCube = GameObject.Find("DummyBox");
        fakeCube.GetComponent<MeshRenderer>().enabled = true;
        transform.position = new Vector3(0.2f, 0.876f, 0.0f);
        switch (tasks)
        {
            //---------NO FORCE-------------------------------------------------------------------//


            //NO FORCE BEFORE - CLOSE
            case 1:
                theCube.transform.position = new Vector3(0.0f, 0.873f, 0.15f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 1;
                break;
            //NO FORCE ON - CLOSE
            case 2:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 2;
                break;
            //NO FORCE BEFORE - FAR
            case 3:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 3;
                break;

            //NO FORCE ON - FAR
            case 4:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 4;
                break;
            //NO FORCE BEFORE - LEFT
            case 5:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 5;
                break;
            //NO FORCE ON - LEFT
            case 6:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 6;
                break;
            //NO FORCE BEFORE - RIGHT
            case 7:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 7;
                break;
            //NO FORCE ON - RIGHT
            case 8:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "NoForce";
                tempTask = 8;
                break;
            default:
                break;


            //---------VIBRATION------------------------------------------------------------------//


            //VIB BEFORE - CLOSE
            /*case 1:
                theCube.transform.position = new Vector3(0.0f, 0.873f, 0.15f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 1;
                break;
            //VIB ON - CLOSE
            case 2:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 2;
                break;
            //VIB BEFORE - FAR
            case 3:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 3;
                break;

            //VIB ON - FAR
            case 4:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 4;
                break;
            //VIB BEFORE - LEFT
            case 5:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 5;
                break;
            //VIB ON - LEFT
            case 6:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 6;
                break;
            //VIB BEFORE - RIGHT
            case 7:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 7;
                break;
            //VIB ON - RIGHT
            case 8:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Vibrating";
                CollisionPush.setVibrate(canVib);
                tempTask = 8;
                break;
            default:
                break;



            //---------FORCE FEEDBACK------------------------------------------------------------//


            //FORCE BEFORE - CLOSE
            case 1:
                theCube.transform.position = new Vector3(0.0f, 0.873f, 0.15f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 1;
                break;
            //FORCE ON - CLOSE
            case 2:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 2;
                break;
            //FORCE BEFORE - FAR
            case 3:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 3;
                break;

            //FORCE ON - FAR
            case 4:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 4;
                break;
            //FORCE BEFORE - LEFT
            case 5:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 5;
                break;
            //FORCE ON - LEFT
            case 6:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 6;
                break;
            //FORCE BEFORE - RIGHT
            case 7:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 7;
                break;
            //FORCE ON - RIGHT
            case 8:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                tempTask = 8;
                break;
            default:
                break;


            //---------FORCE & VIBRATION---------------------------------------------------------//


            //VIBFORCE BEFORE - CLOSE
            case 1:
                theCube.transform.position = new Vector3(0.0f, 0.873f, 0.15f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 1;
                break;
            //VIBFORCE ON - CLOSE
            case 2:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 2;
                break;
            //VIBFORCE BEFORE - FAR
            case 3:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.05f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 3;
                break;

            //VIBFORCE ON - FAR
            case 4:
                theCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                fakeCube.transform.position = new Vector3(0.0f, 0.873f, -0.25f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 4;
                break;
            //VIBFORCE BEFORE - LEFT
            case 5:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 5;
                break;
            //VIBFORCE ON - LEFT
            case 6:
                theCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 6;
                break;
            //VIBFORCE BEFORE - RIGHT
            case 7:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, 0.08579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 7;
                break;
            //VIBFORCE ON - RIGHT
            case 8:
                theCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                fakeCube.transform.position = new Vector3(-0.141421f, 0.873f, -0.18579f);
                theCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                fakeCube.transform.Rotate(0.0f, 0.0f, 0.0f);
                theCube.tag = "Touchable";
                CollisionPush.setVibrate(canVib);
                tempTask = 8;
                break;
            default:
                break;*/
        }
    }
}
