﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionPush : MonoBehaviour
{

    private GenericFunctionsClass genhaptic;

    private bool collided;

    private bool notTouched = true;

    public GameObject cubeVib = GameObject.Find("DeskBox");

    public static bool canVibrate = false;

    /*public float pushStrength = 6.0f, speedupStrength = 10.0f;
    //change to correct force type;
    //probably need to GetProxyPosition();
    private Vector3 vect = GetProxyValues().myProxyTorque;

    void OnCollisionEnter(Collision col)
    {
        Rigidbody rbody = col.collider.attachedRigidbody;

        if (rbody == null || rbody.isKinematic)
        {
            return;
        }

        if (col.moveDirection.y < -0.3f)
        {
            return;
        }

        pushStrength = vect;

        Vector3 direction = new Vector3(col.moveDirection.x, 0, col.moveDirection.z);

        rbody.velocity = direction * pushStrength;

    }

        //CHANGE CUBE, BALL ETC. RIGIDBODY ATTRIB FROM NONE TO INTERPOLATE

        //get torque from the haptic controller

    void Start ()
    {
        vect = gameObject.GetComponent<INSERTHAPTICCONTROLLER>();
    }*/

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Vibrating" || canVibrate)
        {
            collided = true;
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Vibrating" || canVibrate)
        {
            collided = false;
        }
    }

    void Update()
    {
        if (collided && notTouched)
        {
            Sending.sendVibrate();
            //cubevib = GameObject.FindWithTag("Vibrating");
            //cubevib.GetComponent<Rigidbody>().AddForce(Vector3.forward * 5.0f);
            StartCoroutine("DoSomething");
            //notTouched = false;
            //cubeVib.GetComponent<BoxCollider>().enabled = false;
            
        }
        else
        {
            Sending.sendVibrateStop();
        }
    }

    public static void setVibrate(bool vib)
    {
        canVibrate = vib;
    }


    IEnumerator DoSomething()
    {
        yield return new WaitForSeconds(1f);
        //notTouched = false;
        print("Ok");
    }

}
