﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

public class SaveData : MonoBehaviour
{
    public static void Save(string path, string content)
    {
        File.AppendAllText(Application.dataPath + "/" + path, content + Environment.NewLine);
    }

}